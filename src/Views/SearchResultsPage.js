import { useContext } from "react"
import { useLocation } from "react-router-dom"
import Search from "../Components/Search"
import SearchResults from "../Components/SearchResults"
import { LocationContext } from "../provider/LocationProvider"

function SearchResultsPage() {
    const location = useLocation()
    const query = location.state.query
    const locationState = useContext(LocationContext)

    // TODO: find better solution?
    if(location.pathname !== locationState[0]){
        window.location.reload()
    }
    
    return(
        <>
            <Search query={query}/>
            <SearchResults query={query}/>
        </>
    )
}

export default SearchResultsPage
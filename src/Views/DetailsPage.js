import { useContext, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom'
import movieService from '../api/movieService';
import Recommendations from '../Components/Recommendations';
import Similar from '../Components/Similar';
import TitleDetails from '../Components/TitleDetails'
import WatchProviders from '../Components/WatchProviders';
import { LocationContext } from '../provider/LocationProvider';
function DetailsPage () {
    const location = useLocation();
    const [details, setDetails] = useState()
    const [watchProviders, setWatchProviders] = useState()
    const lstate = useContext(LocationContext)
    const id = location.state.id
    const mediaType = location.state.mediaType

    async function getWatchProviders () {
        try {   
            const {data} = await movieService.fetchWatchProviders(id, mediaType)
            setWatchProviders(data.results)
            
        } catch (error) {
            
        }
    }

    async function fetchDetails ()  {
        try {
            const {data} = await movieService.fetchMediaDetails(id, mediaType)
            setDetails(data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        fetchDetails()
        getWatchProviders()
    }, [lstate])

    return (
        <>
            { details ? <TitleDetails titleDetails={details}  mediaType={location.state.mediaType}/> : <p>Loading...</p>}
            { watchProviders ? <WatchProviders providers={watchProviders}/ >: <p>Loading watch providers...</p>}
            {/* <Similar id={id} mediaType={mediaType} /> */}
            <Recommendations id={id} mediaType={mediaType} />
        </>
    )
}

export default DetailsPage
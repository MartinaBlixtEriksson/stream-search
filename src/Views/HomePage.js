import axios from "axios"
import { useContext, useEffect, useState } from "react"
import Search from "../Components/Search"
import Titles from "../Components/Titles"
import { CountryContext } from "../provider/CountryProvider"

function HomePage() {
    const [ipInfo, setIpInfo] = useContext(CountryContext)
    const [loading, setLoading] = useState(true)

    const fetchData = async () => {
        try {
            const {data }= await axios.get('http://ip-api.com/json/')
            setIpInfo(data.countryCode)
            setLoading(false)
            
        } catch (error) {
            setLoading(false)
            console.log("Error while fetching country");
        }
    }
 useEffect(() => {
    fetchData()
 }, [])

    return(
        <>
            <Search />
            <Titles />
        </>
    )
}

export default HomePage
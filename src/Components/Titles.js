import { useEffect, useState } from "react"
import movieService from '../api/movieService'
import TitlesArray from "./TitlesArray"

function Titles() {
    const [trending, setTrending] = useState([])
    const [error, setError] = useState()
    const [loading, setLoading] = useState(true)
    
    const fetchData = async () =>{
        try {
            const { data } = await movieService.fetchTrending()
            setLoading(false)
                setTrending(data.results)
        } catch (error) {
            setLoading(false)
            setError("Can't load data")
        }
    }

    useEffect(() => {
        fetchData()
    }, [])
    useEffect(() => {
    })

    if(error){
        return <div>Error: {error.message}</div>
    } else if (loading){
        return <div>loading</div>
    } else{

        return(
           <TitlesArray props = {trending} title="Trending"></TitlesArray>
        )
    }

    
    
    

}

export default Titles
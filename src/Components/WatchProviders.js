import { useContext } from "react"
import { CountryContext } from "../provider/CountryProvider"

function WatchProviders (props) {

    let country = useContext(CountryContext)
    let providers = props.providers[country[0]]

    if (providers && providers.flatrate){
        return(
            <>
                <h3>Flatrate</h3>
                <ul>
                    {Object.values(providers.flatrate).map((value, index) => (
                        <li key={index}>
                            <img src={`https://image.tmdb.org/t/p/original${value.logo_path}` } alt="" height={64} />
                        </li>
                    ))}
                </ul>
            </>
        )
    } if(providers && providers.buy) {
        return(
            <>
                <h3>Buy</h3>
                <ul>
                    {Object.values(providers.buy).map((value, index) => (
                        <li key={index}>
                            <img src={`https://image.tmdb.org/t/p/original${value.logo_path}` } alt="" height={64} />
                        </li>
                    ))}
                </ul>
            </>
        )
    } if(providers && providers.rent){
        return(
            <>
                <h3>Rent</h3>
                <ul>
                    {Object.values(providers.rent).map((value, index) => (
                        <li key={index}>
                            <img src={`https://image.tmdb.org/t/p/original${value.logo_path}` } alt="" height={64} />
                        </li>
                    ))}
                </ul>
            </>
        )
    }   else{
            return(
                <p>No streaming providers found for country: {country}</p>
            )
        
    }
}

export default WatchProviders
import { useEffect, useState } from "react"
import movieService from "../api/movieService"
import TitlesArray from "./TitlesArray"
// ! Fungerar ej eftersom det inte finns någon mediaType.

function Similar(props) {
    const [similar, setSimilar] = useState()
    const [loading, setLoading] = useState(true)

    async function getSimilar(){
        try {
            const {data} = await movieService.fetchSimilarTitles(props.id, props.mediaType)
            setLoading(false)
            setSimilar(data.results)
            console.log(data.results)
            
        } catch (error) {
            setLoading(false)
            console.log(error)
        }

    }

    useEffect(() => {
        getSimilar()
    }, [])

    if(loading) {
        return <div>Loading similar titles...</div>
    }
    return (
        <>
            <TitlesArray props={similar} title="Similar" />
        </>
    )
}

export default Similar
import { ListGroup } from "react-bootstrap";

function TitleDetails (data) {

    function convertTime(totalMinutes){
        const hours = Math.floor(totalMinutes / 60)
        const minutes = totalMinutes % 60

        return (`${hours}h ${minutes}min`)
    }

    const details = data.titleDetails
    console.log(details);
    return(
        <> 
            <h2> { details.hasOwnProperty('name') ? details.name : details.original_title } </h2>
            <p>{details.tagline}</p>
            {convertTime(details.runtime)}
            <p>{details.release_date}</p>
            <ListGroup horizontal className="mb-2">
                {Object.values(details.genres).map((key, index) => 
                    <ListGroup.Item key={index}>
                        {key.name}
                    </ListGroup.Item>
                    )}
            
            </ListGroup>
            
            <img src={`https://image.tmdb.org/t/p/original${details.poster_path}` } alt="" height={500} />

            <div className="w-50">{details.overview}</div>
        </>
    )
}

export default TitleDetails
import { useEffect, useState } from "react"
import movieService from "../api/movieService"
import TitlesArray from "./TitlesArray"

function SearchResults (props) {

    const [searchResults, setSearchResults] = useState()
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState()

    async function fetchData () {
        try {
            const {data} = await movieService.fetchSearchResults(props.query)
            setLoading(false)
            setSearchResults(data.results)
        } catch (error) {
            setLoading(false)
            setError(error)
        }
    }

    useEffect(() =>{
        fetchData()
    }, [])

    if(loading) {
        return <div>Loading...</div>
    } else if(error){
        return <div>Error: {error.message}</div>
    }
    return(
        <>
            { searchResults.length > 0 &&
                <TitlesArray props={searchResults} title="Results"/>
            }
        </>
    )
}

export default SearchResults
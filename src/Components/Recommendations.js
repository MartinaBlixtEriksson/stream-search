import { useEffect, useState } from "react"
import movieService from "../api/movieService"
import TitlesArray from "./TitlesArray"

function Recommendations(props) {
    const [recommendations, setRecommendations] = useState()
    const [loading, setLoading] = useState(true)

    async function getRecommendations() {
        try {
            const {data} = await movieService.fetchRecommendations(props.id, props.mediaType)
            setLoading(false)
            setRecommendations(data.results)
            
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    useEffect(() => {
        getRecommendations()
    }, [])

    if(loading){
        return <div>Loading..</div>
    }
    return(
        <>  
            { recommendations.length > 0 &&
            <TitlesArray props={recommendations} title="Recommendations" />
            }

        </>
    )
}

export default Recommendations
import { useState } from "react"
import { Button, Form } from "react-bootstrap"
import { useNavigate } from "react-router-dom"

function Search(props){
    const [searchQuery, setSearchQuery] = useState()
    const navigate = useNavigate()

    async function submitSearch (e) {
        e.preventDefault()
        navigate(`/searchResults/${searchQuery}`, {state: {query: searchQuery}})
    }

    return (
        <>
            <Form onSubmit={submitSearch}>
                <Form.Group className="mb-3" controlId="formBasicSearch">
                    <Form.Label>Search</Form.Label>
                    { props.query ? 
                    <Form.Control type="text" defaultValue={props.query} onChange={e => setSearchQuery(e.target.value)}></Form.Control> :
                    <Form.Control type="text" placeholder="Search for TV shows, movies, people" onChange={e => setSearchQuery(e.target.value)}></Form.Control>
                    }
                </Form.Group>
                <Button variant="primary" type="submit" >
                    Submit
                </Button>
            </Form>
        </>
    )
}

export default Search
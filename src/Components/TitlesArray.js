import { useContext } from "react"
import { Link } from "react-router-dom"
import { LocationContext } from "../provider/LocationProvider"
import noPicture from '../assets/no-pictures.png'
import Carousel from "./Carousel/Carousel"

function TitlesArray (data) {
    const  [location, setLocation] = useContext(LocationContext)

    function onTitleClick (){
        setLocation(window.location.pathname)
    }

    return (
        <>
            <h1>{data.title}</h1>
                <Carousel
                show={5}
                infiniteLoop={true}
                >

                {Object.values(data.props).map((value, index) => (
                    <div key={index}>
                    <Link to={`/details/${value.id}`} state={{id: value.id, mediaType: value.media_type}}>
                        {(!Object.hasOwn(value, 'poster_path') || value.poster_path === null) ?
                        <>
                        <img src={noPicture} alt="Not found" height="200">
                        </img>
                        <p>{value.name}</p>
                        </>
                        : 
                        <img src={`https://image.tmdb.org/t/p/original${value.poster_path}`} alt="Title poster" height="300"
                        onClick={() => onTitleClick(value.id, value.media_type)}>   
                        </img>
                        }
                    </Link>
                </div>

                )
                )}
                </Carousel>

        </>
    )
}

export default TitlesArray
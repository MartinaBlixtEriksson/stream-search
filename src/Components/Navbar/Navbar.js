import { NavLink } from 'react-router-dom'
import cinema from '../../assets/cinema.png'
import './Navbar.css'

function Navbar () {
    return (
        <>   
            <NavLink to="/" className="ms-2 nav-link"> 
                <img src={cinema} alt="" height={50} />
                <h1 className='nav-title'  >Stream Search</h1>
            </NavLink>

        </>
    )
}

export default Navbar
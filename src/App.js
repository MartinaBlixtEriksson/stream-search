
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Navbar from './Components/Navbar/Navbar';
import { CountryProvider } from './provider/CountryProvider';
import { LocationProvider } from './provider/LocationProvider';
import DetailsPage from './Views/DetailsPage';
import HomePage from './Views/HomePage';
import SearchResultsPage from './Views/SearchResultsPage';

function App() {
  return (
    <>
      <BrowserRouter>
        <CountryProvider>
          <Navbar />
          <LocationProvider>
            <div className='ms-4'>
            <Routes>
              <Route exact path='/' element={<HomePage/>} />
                <Route exact path='/details/:id' element={<DetailsPage/>} name="details" />
              <Route path='/searchResults/:slug' element={<SearchResultsPage />} />
            </Routes>
            </div>
          </LocationProvider>
        </CountryProvider>
      </BrowserRouter>
    </>
  );
}

export default App;

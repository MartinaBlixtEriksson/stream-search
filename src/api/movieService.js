import http from './axios'
const API_KEY = process.env.REACT_APP_MOVIEDB_KEY

const fetchTrending = () => {
    return http.get(`/trending/all/day?api_key=${API_KEY}`)
}

const fetchMediaDetails = (movieId, mediaType) => {
    return http.get(`/${mediaType}/${movieId}?api_key=${API_KEY}`)
}

const fetchWatchProviders = (movieId, mediaType) => {
    return http.get(`/${mediaType}/${movieId}/watch/providers?api_key=${API_KEY}`)
}

const fetchRecommendations = (mediaId, mediaType) => {
    return http.get(`/${mediaType}/${mediaId}/recommendations?api_key=${API_KEY}&language=en-US&page=1`)
}

const fetchSimilarTitles = (mediaId, mediaType) => {
    return http.get(`/${mediaType}/${mediaId}/similar?api_key=${API_KEY}&language=en-US&page=1`)
}

const fetchSearchResults = (query) => {
    return http.get(`/search/multi?api_key=${API_KEY}&query=${query}`)
}

export default { fetchTrending, fetchMediaDetails, fetchWatchProviders, fetchRecommendations, fetchSimilarTitles, fetchSearchResults }